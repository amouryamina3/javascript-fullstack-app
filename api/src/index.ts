import { createServer } from "./server";
import { PORT, Node_ENV } from "./config";

import {prisma } from './database/index';



const main = async () => {
    const server = createServer();

    const posts = await prisma.post.findMany()
    console.log(posts);
    

    //console.log(process.env.PORT);
    
    
    server.listen(process.env.PORT,() => {
        console.log(`Server is running in port  ${PORT} in ${Node_ENV} mode`);
        
    })
}

main();
