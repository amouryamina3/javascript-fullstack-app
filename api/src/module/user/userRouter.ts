import {Router} from 'express'

import { createUserController } from './useCases/createUser';
import { loginController } from './useCases/login/index'

const userRouter: Router = Router ();

console.log('USER ROUTER');

// Post -> http://localhost:3000/api/v2/users/
userRouter.post('/',(req, res)=> createUserController.execute(req, res))


//Authenticate
userRouter.post('/authenticate', (req, res) => loginController.execute(req, res))

export {userRouter}