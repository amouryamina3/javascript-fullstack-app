// le controller gére la logique de réception d'une requête et gère la logique d'envoie d'une réponse
// Gère la logique HTTP
// Post ->http://localhost:3000/api/v2/users/
// Get ->http://localhost:3000/api/v2/users/
// Delete ->http://localhost:3000/api/v2/users/:id
// Update ->http://localhost:3000/api/v2/users/
// =>les routes correspondantes auxx CRUD seront toujours selon ce format (méthode+nom du model)

import { CreateUser } from './createUser'
import { Request, Response } from 'express'
// import { validate } from 'class-validator'

//DTO
// import { RequestCreateUserDto } from './createUserDto'

//Controller
export class CreateUserController {
    private useCase: CreateUser;

    constructor(createUser: CreateUser) {
        this.useCase = createUser;
    }

    public async execute(req: Request, res: Response) {


        console.log('controller body', req.body );
        
        // body {
        //     email:"qsdqsd@qsdqs.com"
        //     password:"*******"
        // }

        // const requestUserDto = new RequestCreateUserDto(req.body);
        // const errors = await validate(requestUserDto);


        // console.log('Request DTO create user errors : ', errors);

        // const dtoErrors = await requestUserDto.isValid(requestUserDto)

        // if (!!dtoErrors) {
        //     return res.status(400).json(dtoErrors);
        // }

        try {
            //Appel au service
            const result = await this.useCase.execute(req.body);


            if (!result.success) {
                return res.status(400).json({ message: result.message })
            }

            console.log('result', result);

            return res.status(201).json();
        }
        catch (err) {
            // console.log('create controllers errors :', err);
            return res.status(400).json({ message: err })
        }

    }
}