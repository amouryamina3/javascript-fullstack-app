import express from 'express'
import cors from 'cors'

import { mainRouter } from './router'
import { APP_BASE_URL } from './config';

export const createServer = () => {
    const server: express.Application = express();

    server.use(cors({
        origin:"http://localhost:1234" //le port de notre front
    }
    ))

    server.use(express.json())

    server.use(APP_BASE_URL, mainRouter )

    return server
}