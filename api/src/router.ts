import { prisma } from './database/index'
import { Router, Request, Response } from 'express'


import { userRouter } from './module/user/userRouter';

const mainRouter: Router = Router();

//Voilà, notre première route !
mainRouter.get('/', (_: Request, res: Response) => {
    res.json('Hello CPROM');
})

mainRouter.get('/posts', async (_: Request, res: Response) => {
    const posts = await prisma.post.findMany();
    res.send(posts);
})

mainRouter.use('/users', userRouter)

export default mainRouter

 

 export { mainRouter }
